﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Migrations.Model;
using System.Data.Entity.Migrations.Sql;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EFRemont
{
	public class Request
	{
		public int Id { get; set; }
		// Device
		public string DeviceType  { get; set; }
		public string DeviceSN    { get; set; }
		public string DeviceBrend { get; set; }
		public string DeviceModel { get; set; }
		public string DeviceFault { get; set; }

		public string Description { get; set; }
		// Ориентировочная дата необходима для заполнения
		[Required]
		[DataType(DataType.DateTime)]
		public DateTime? Deadline { get; set; } = null;
		public DateTime? DateStart { get; set; } = DateTime.Now;
		public DateTime? DateEnd { get; set; } = null;
		
		public decimal PriceApproximate { get; set; } = 0;
		public decimal PrePayment { get; set; } = 0;
		public decimal SumPay => CashTransactions?.Where(w => w.SFlag).Sum(s => s.Cash) ?? 0;
		public decimal SumPayAll => SumPay + PrePayment;
		public decimal SumWorks => Works?.Sum(s => s.Sum) ?? 0;
		public decimal TotalSum => PriceApproximate + SumWorks;

		public bool Quickly { get; set; }
		public int Status { get; set; } = 1;
		public bool SFlag { get; set; } = true;
		public DateTime DateStamp { get; set; } = DateTime.Now;

		public DateTime CreateDate { get; set; } = DateTime.Now;

		public int? ClientId { get; set; }
		[Required]
		public Client Client { get; set; }

		public List<Work> Works { get; set; } = new List<Work>();

		public List<CashBoxMove> CashTransactions { get; set; }
	}

	public class Client
	{
		public int Id { get; set; }
		[Required]
		[DisplayName("Клиент")]
		public string Name { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		public string Address { get; set; }
		public string Details { get; set; }
		public int RequestCount => Requests?.Count(x => x.SFlag) ?? 0;

		public DateTime? DateStamp { get; set; } = DateTime.Now;
		public DateTime CreateDate { get; set; } = DateTime.Now;
		public bool SFlag { get; set; } = true;

		public List<Request> Requests { get; set; }
	}

	public class CashBoxMove
	{
		public int Id { get; set; }
		public decimal Cash { get; set; } = 0;
		public string Details { get; set; }

		public DateTime? DateStamp { get; set; } = DateTime.Now;
		public DateTime CreateDate { get; set; } = DateTime.Now;
		public bool SFlag { get; set; } = true;

		[ForeignKey("Request")]
		public int? RequestId { get; set; }
		public Request Request { get; set; }
	}

	public class Work
	{
		public int Id { get; set; }
		[Required]
		public string Name { get; set; }
		[Required]
		public decimal? Price { get; set; }
		public int? Count { get; set; } = 1;
		public string Description { get; set; }
		public decimal? Sum => Price * Count;
		public bool IsWork { get; set; } = false;

		public DateTime? DateStamp { get; set; } = DateTime.Now;

		public DateTime CreateDate { get; set; } = DateTime.Now;
		public bool SFlag { get; set; } = true;

		[ForeignKey("Request")]
		public int? RequestId { get; set; }
		public Request Request { get; set; }
	}
}
