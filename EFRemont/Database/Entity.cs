﻿namespace EFRemont
{
	using System.Data.Entity;
	
	public class Entity : DbContext
	{
		public Entity():
			base("Data Source=<>;Initial Catalog=EFRemont;User ID=<>;Password=<>;Encrypt=True;Connection Timeout=10")
		{
		}

		public DbSet<Client> Clients { get; set; }
		public DbSet<Request> Requests { get; set; }
		public DbSet<Work> Works { get; set; }
		public DbSet<CashBoxMove> CashBoxMove { get; set; }
	}
}
