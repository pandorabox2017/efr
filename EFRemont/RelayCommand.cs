﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace EFRemont
{
    class RelayCommand : ICommand
    {
        private Action<object> _execute;

        public event EventHandler CanExecuteChanged;
        public bool CanExecute(object parameter) => true;

        public void Execute(object parameter) => _execute(parameter);
        public RelayCommand(Action<object> execute) => _execute = execute;
    }
}
