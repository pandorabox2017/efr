﻿using MahApps.Metro.IconPacks;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace EFRemont
{
	class FlyRequestHeaderConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture) => (int)value > 0 ? $"Изменение заказа #{(int)value}" : "Добавление заказа";
		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class FlySourceTypeConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture) => (bool)value ? "р" : "м";

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class FlyClientHeaderConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture) => (int)value > 0 ? $"Клиент #{(int)value}" : "Добавление клиента";

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class FlyCashHeaderConver : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture) => (int)value > 0 ? $"Транзакция #{(int)value}" : "Добавление транзакции";

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class CashColorConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture)
		=> (decimal)value > 0
			? Colors.DarkGreen.ToString() // Green
			: Colors.DarkRed.ToString(); // Red

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class RequestQuickConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture)
		=> (bool)value ? Visibility.Visible : Visibility.Collapsed;

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class RequestStatusTextConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture)
		=> (int)value == 1 ? "В работе" : "Закрыт";

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
	class RequestStatusColorConvert : IValueConverter
	{
		public object Convert(object value, Type tType, object param, CultureInfo culture)
		=> (int)value == 1
			? Colors.LightGreen.ToString()
			: Colors.LightGray.ToString();

		public object ConvertBack(object value, Type tType, object param, CultureInfo culture) => DependencyProperty.UnsetValue;
	}
    class WorkStatusButtonConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        => (int)value == 0 ? "Добвавить" : "Изменить";

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => DependencyProperty.UnsetValue;
    }
}
