﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.Diagnostics;
using System.Threading;
using System.Globalization;
using System.Drawing;
using System.Windows.Threading;

namespace EFRemont
{
	public class CreateReport
	{
		#region Define data
		private Entity db;
		private MainWindow mainWnd;

		private string search;
        private bool IsSearch => !string.IsNullOrWhiteSpace(search);
		private bool IsRequestClient;
		private bool IsCashClient;
		private DateTime? startPeriod;
		private DateTime? endPeriod;
		private int requestState;
		
		private string file;
		const string STYLE_LINK = "Hyperlink";
		private CultureInfo ru = new CultureInfo("ru-ru");
		#endregion

		#region Define query
		private Expression<Func<Request, bool>> FilterRequest => r => r.SFlag
					&& (!IsSearch || (r.Id.ToString().Contains(search) 
					                  || r.Client.Name.ToUpper().Contains(search)))
					&& (!startPeriod.HasValue || (r.DateStart > startPeriod || r.DateEnd > startPeriod))
					&& (!endPeriod.HasValue || (r.DateStart < endPeriod || r.DateEnd < endPeriod))
					&& (requestState == 1 ? r.Status == 1 :
						requestState != 2 || r.Status == 0);
		private Expression<Func<Client, bool>> FilterClient => x => x.SFlag
					&& (!IsSearch || (x.Id.ToString().Contains(search)
					                  || x.Name.ToUpper().Contains(search)
					                  || x.Email.ToUpper().Contains(search) 
					                  || x.Address.ToUpper().Contains(search)
					                  || x.Phone.ToUpper().Contains(search)))
					&& (!startPeriod.HasValue || (x.DateStamp > startPeriod))
					&& (!endPeriod.HasValue || (x.DateStamp < endPeriod));
		private IQueryable<Request> ReportQuery =>
			db.Requests
			.AsNoTracking()
			.Include(i => i.Client)
			.Include(i => i.CashTransactions)
			.Include(i => i.Works)
			.Where(FilterRequest)
			.OrderBy(x => x.Id)
			.Select(x => x);
		private IQueryable<Client> ReportClientQuery =>
			db.Clients
			.AsNoTracking()
			.Include(i => i.Requests.Select(k => k.CashTransactions))
			.Include(i => i.Requests.Select(k => k.Works))
			.Where(FilterClient)
			.OrderBy(x => x.Id)
			.Select(x => x);

		private static void ColAction(Action<int> action, params int[] cols) => cols.ToList().ForEach(action);
		#endregion

		private Task _taskCreateReport, _taskCreateClient;

		public CreateReport(Entity _db, MainWindow _window, string file)
		{
			db = _db;
			mainWnd = _window;
			search = _window.txtSearchReport.Text.ToUpper();
			startPeriod = _window.dtRepStart.SelectedDate;
			endPeriod = _window.dtRepEnd.SelectedDate;
			requestState = _window.cbRequestReportState.SelectedIndex;
			IsRequestClient = mainWnd.IsRequestForClientsToReport.IsChecked.Value;
			IsCashClient = mainWnd.IsDetailRequestForClientsToReport.IsChecked.Value && IsRequestClient;

			this.file = file;
		}

		public void PublicRequests()
		{
			mainWnd.Btn_Export.IsEnabled = false;
			_taskCreateReport = new Task(Requests);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			_taskCreateReport
				.ContinueWith(t =>
				{
					t.DispatcherAction(mainWnd, async () => await mainWnd.MsgBox(t.Exception.InnerException.Message));
				}, TaskContinuationOptions.OnlyOnFaulted)
				.ContinueWith(t => t.DispatcherAction(mainWnd, () => mainWnd.Btn_Export.IsEnabled = true))
				.ContinueWith(t =>
				{
					GC.Collect();
					GC.WaitForPendingFinalizers();
					GC.Collect();
				});
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			_taskCreateReport.Start();
		}

		private void Requests()
		{
			Thread.CurrentThread.CurrentCulture = ru;
			FileStream fs = null;
			try
			{
				fs = File.Open(file, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
				using (var pck = new ExcelPackage(fs))
				{
					var workSheet = pck.Workbook.Worksheets.Add("Заказ");

					if (ReportQuery == null || ReportQuery?.Count() == 0)
					{
						throw new Exception("Отсутствуют данные для выгрузки");
					}
					else if (ReportQuery.Count() == 1)
					{
						var reports = ReportQuery.ToList();
						//var report = reports.AbuseReportQuery();
						workSheet.Name = $"Заказ {reports.First().Id}";
						CreateOneSheetReport(workSheet, reports.First());
					}
					else
					{
						var reports = ReportQuery.AsParallel().AsOrdered();
						CreateStyle(pck);

						workSheet.Name = $"Заказы {DateTime.Now.ToString("g", ru)}";

						#region Create head
						var header = workSheet.Cells[1, 1, 1, 6];
						header.Merge = true;
						header.Value = $"Отчёт состояния заказов на {DateTime.Now.ToString("G", ru)}";

						workSheet.Cells[3, 1].Value = "Номер";
						workSheet.Cells[3, 2].Value = "Статус заказа";
						workSheet.Cells[3, 3].Value = "Клиент";
						workSheet.Cells[3, 4].Value = "Предоплата";
						workSheet.Cells[3, 5].Value = "Выплаченная сумма";
						workSheet.Cells[3, 6].Value = "Дата оформления заказа";

						workSheet.Cells[1, 1, 3, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
						#endregion

						var rows = 4;
						foreach (var i in reports)
						{
							workSheet.Cells[rows, 1].Value = i.Id;
							workSheet.Cells[rows, 1].StyleName = STYLE_LINK;

							workSheet.Cells[rows, 2].Value = i.Status == 1 ? "Открыт" : "Закрыт";
							workSheet.Cells[rows, 3].Value = i.Client.Name;
							workSheet.Cells[rows, 4].Value = i.PrePayment;
							workSheet.Cells[rows, 5].Value = i.SumPayAll;
							workSheet.Cells[rows, 6].Value = i.CreateDate;
							var link = $"Заказ {i.Id}";
							var ws = pck.Workbook.Worksheets.Add(link);
							workSheet.Cells[rows++, 1].Hyperlink = new Uri($"#'{link}'!A1", uriKind: UriKind.Relative);

							CreateOneSheetReport(ws, i);
						};

						var border = workSheet.Cells[3, 1, --rows, 6];
						border.Style.Border.Top.Style = ExcelBorderStyle.Thin;
						border.Style.Border.Left.Style = ExcelBorderStyle.Thin;
						border.Style.Border.Right.Style = ExcelBorderStyle.Thin;
						border.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
						border.AutoFitColumns();

						ExcelRange range(int col) => workSheet.Cells[4, col, rows, col];
						ColAction(c => range(c).Style.Numberformat.Format = "dd.mm.yyyy", 6);
						ColAction(c => range(c).Style.Numberformat.Format = "# ##0.00 ₽", 4, 5);
						ColAction(c => range(c).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center, 1, 2, 6);
					}
					pck.Save();
					OpenFile(file);
				}
			}
			catch (IOException ex)
			{
				throw new Exception($"Не удалось открыть\\создать файл\n\n{ex.Message}");
			}
			catch (Exception ex)
			{
				throw new Exception($"Ошибка в формировании отчёта\n\n{ex.InnerException?.Message ?? ex.Message}");
			}
			finally
			{
				fs?.Close();
			}
		}



		private void OpenFile(string file)
		{
			Process.Start("excel.exe", $"/r \"{file}\"");
		}

		void CreateStyle(ExcelPackage ep)
		{
			var s = ep.Workbook.Styles.CreateNamedStyle(STYLE_LINK);
			s.Style.Font.UnderLine = true;
			s.Style.Font.Color.SetColor(Color.Blue);
			s.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
		}
		private void CreateOneSheetReport(ExcelWorksheet ews, Request r)
		{
			InfoRequestTable(1, 1, r, ews);
			var colRelative = 4;
			if (r.Works?.Count > 0)
			{
				WorksTable(1, colRelative, r.Works, ews);
				colRelative += 6;
			}
			if (r.CashTransactions?.Count > 0)
				CashTable(1, colRelative, r.CashTransactions, ews);
		}

		public void PublicClients()
		{
			mainWnd.Btn_Export.IsEnabled = false;

			_taskCreateClient = new Task(Clients);
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			_taskCreateClient
				.ContinueWith(t => t.DispatcherAction(mainWnd, () => mainWnd.MsgBox(t.Exception.InnerException.Message)), TaskContinuationOptions.OnlyOnFaulted)
				.ContinueWith(t =>
				{
					GC.Collect();
					GC.WaitForPendingFinalizers();
					GC.Collect();
				})
				.ContinueWith(t => t.DispatcherAction(mainWnd, () => mainWnd.Btn_Export.IsEnabled = true));
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
			_taskCreateClient.Start();
		}

		private void Clients()
		{
			Thread.CurrentThread.CurrentCulture = ru;
			FileStream fs = null;
			try
			{
				fs = File.Open(file, FileMode.Create, FileAccess.ReadWrite, FileShare.None);
				using (var pck = new ExcelPackage(fs))
				{
					var mainSheet = pck.Workbook.Worksheets.Add("Клиенты");

                    lock (db)
                    {
						if (ReportClientQuery == null || ReportClientQuery?.Count() == 0)
						{
							throw new Exception("Отсутсвуют данные для выборки");
						}
                        else if (ReportClientQuery.Count() == 1)
                        {
							var reports = ReportClientQuery.ToList();
							mainSheet.Name = $"Клиент {reports.First().Id}";
							CreateOneSheetClient(mainSheet, reports.First());
                        }
                        else
                        {
                            var reports = ReportClientQuery.AsParallel().AsOrdered();
                            CreateStyle(pck);

                            mainSheet.Name = $"Клиенты {DateTime.Now.ToString("g", ru)}";

                            #region head
                            var header = mainSheet.Cells[1, 1];
                            header.Value = $"Отчёт состояния клиентов на {DateTime.Now.ToString():G}";

                            mainSheet.Cells[3, 1].Value = "Номер";
                            mainSheet.Cells[3, 2].Value = "Наименование";
                            mainSheet.Cells[3, 3].Value = "Количество заказов";
                            mainSheet.Cells[3, 1, 3, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            #endregion

                            var row = 4;
                            foreach (var i in reports)
                            {
                                mainSheet.Cells[row, 1].Value = i.Id;
                                mainSheet.Cells[row, 1].StyleName = STYLE_LINK;

                                mainSheet.Cells[row, 2].Value = i.Name;
                                mainSheet.Cells[row, 3].Value = i.RequestCount;

                                var link = $"Клиент {i.Id}";
                                var ws = pck.Workbook.Worksheets.Add(link);
                                mainSheet.Cells[row++, 1].Hyperlink = new Uri($"#'{link}'!A1", UriKind.Relative);

                                CreateOneSheetClient(ws, i);
                            }
                            var border = mainSheet.Cells[3, 1, --row, 3];
                            var bStyle = border.Style.Border;
                            bStyle.Top.Style = ExcelBorderStyle.Thin;
                            bStyle.Left.Style = ExcelBorderStyle.Thin;
                            bStyle.Right.Style = ExcelBorderStyle.Thin;
                            bStyle.Bottom.Style = ExcelBorderStyle.Thin;
                            border.AutoFitColumns();
                        } 
                    }
					pck.Save();
					OpenFile(file);
				}
			}
			catch (IOException ex)
			{
				throw new Exception($"Не удалось открыть\\создать файл\n\n{ex.Message}");
			}
			catch (Exception ex)
			{
				throw new Exception($"Ошибка в формироании отчёта\n\n{ex.InnerException?.Message}");
			}
			finally
			{
				fs?.Close();
			}
		}

		private void CreateOneSheetClient(ExcelWorksheet ws, Client i)
		{
			int InfoTable(int row, int col, Client s)
			{
				#region Header
				var rowHeader = row;
				ws.Cells[row, col, row, col + 1].Merge = true;
				ws.Cells[row, col].Value = "Информация по клиенту";
				ws.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
				#endregion

				#region Data
				var info = new Dictionary<string, string>
					{
						{"ИД", s.Id.ToString() },
						{"Наименование", s.Name },
						{"Телефон", s.Phone },
						{"E-mail", s.Email },
						{"Адрес", s.Address },
						{"Дополнительно", s.Details },
						{"Количество заказов", s.RequestCount.ToString() },
						{"Дата добавления", $"{s.CreateDate.ToShortDateString()} {s.CreateDate.ToShortTimeString()}" }
					};
				info.ToList().ForEach(q =>
				{
					ws.Cells[++row, col].Value = q.Key;
					ws.Cells[row, col + 1].Value = q.Value;
				});
				var table = ws.Cells[rowHeader, col, row, col + 1];
				var tStyle = table.Style.Border;
				tStyle.Top.Style = ExcelBorderStyle.Thin;
				tStyle.Left.Style = ExcelBorderStyle.Thin;
				tStyle.Right.Style = ExcelBorderStyle.Thin;
				tStyle.Bottom.Style = ExcelBorderStyle.Thin;
				table.AutoFitColumns();
				#endregion

                return row - rowHeader;
			}
			int RequestsTeble(int row, int col, List<Request> s, ExcelWorksheet ew)
			{
				var _row = row + 2;
				s.ForEach(r =>
				{
					_row += InfoRequestTable(_row, 1, r, ew) + 3;
					if (r?.Works.Count > 0)
						_row += WorksTable(_row, 1, r.Works, ew) + 3;
                    if (IsCashClient && !(r.CashTransactions is null) && r.CashTransactions.Count > 0)
                        _row += CashTable(_row, 1, r.CashTransactions, ew) + 3;
                    _row += 2;
				});
                return _row;
			}

			var rows = InfoTable(1, 1, i);
            if (IsRequestClient && i.RequestCount > 0)
                rows += RequestsTeble(9, 1, i.Requests, ws);
            ws.Cells[1, 1, rows, 5].AutoFitColumns();
		}

		public int InfoRequestTable(int row, int col, Request s, ExcelWorksheet ews)
		{
			#region Data table
			var info = new Dictionary<string, string>
						{
							{"Статус",                  s.Status == 1 ? "Открыт" : "Закрыт" },
							{"Срочно",                  s.Quickly ? "Да" : "Нет" },
							{"Клиент",                  s.Client.Name },
							{"Предоплата",              s.PrePayment.ToString("C2") },
							{"Сумма по транзакциям",    s.SumPay.ToString("C2") },
							{"Выплаченная сумма",       s.SumPayAll.ToString("C2") },
							{"Дата оформления",         $"{s.CreateDate.ToShortDateString()} {s.CreateDate.ToShortTimeString()}" },
							{"Описание",                s.Description },
							{"Ориентировочная цена",    s.PriceApproximate.ToString("C2") },
							{"Серийный номер",          s.DeviceSN },
							{"Бренд",                   s.DeviceBrend },
						};
            #region Header
            var numReq = ews.Cells[row, col];
            numReq.IsRichText = true;
            numReq.RichText.Add("Номер заказа ");
            numReq.RichText.Add($"#{s.Id.ToString()}").Bold = true;
            numReq.RichText.ToList().ForEach(t =>
            {
                t.Size = 14;
                t.FontName = "Calisto MT";
            });
            var rowHeader = row + 1; 
            #endregion

            info.ToList().ForEach(i =>
			{
				ews.Cells[++row, col].Value = i.Key;
				ews.Cells[row, col + 1].Value = i.Value;
			});
			var table = ews.Cells[rowHeader, col, row, col + 1];
			table.AutoFitColumns();
			table.Style.Border.Top.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
			#endregion
			return row - rowHeader;
		}
		public int WorksTable(int row, int col, List<Work> w, ExcelWorksheet ews)
		{
			#region Header table
			var rowHeader = row;
			var headers = new string[]
			{
				"Наименование",
				"Тип",
				"Количество",
				"Цена",
				"Сумма",
				"Дата исполнения",
			};
			ews.Cells[row, col, row, col + headers.Count() - 1].Merge = true;
			ews.Cells[row, col].Value = "Информация по услугам";
			ews.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
			row++;
			(Enumerable.Range(0, headers.Count())).ToList().ForEach(c =>
			{
				ews.Cells[row, col + c].Value = headers[c];
				ews.Cells[row, col + c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
			});
			#endregion

			#region Data table
			w.ForEach(k =>
			{
				row++;
				ews.Cells[row, col].Value = k.Name;
				ews.Cells[row, col + 1].Value = k.IsWork ? "Работа" : "Материал";
				ews.Cells[row, col + 2].Value = k.Count;
				ews.Cells[row, col + 3].Value = k.Price.Value.ToString("C2");
				ews.Cells[row, col + 4].Value = k.Sum.Value.ToString("C2");
				ews.Cells[row, col + 5].Value = $"{k.CreateDate.ToShortDateString()} {k.CreateDate.ToLongTimeString()}";
			});
			#endregion
			var table = ews.Cells[rowHeader, col, row, col + headers.Count() - 1];
			table.AutoFitColumns();
			table.Style.Border.Top.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
			return row - rowHeader;
		}
		public int CashTable(int row, int col, List<CashBoxMove> s, ExcelWorksheet ews)
		{
			#region Header table
			var rowHeader = row;
			var headers = new string[]
			{
							"Дата проведения",
							"Сумма операции",
							"Детали платежа"
			};
			ews.Cells[row, col, row, col + headers.Count() - 1].Merge = true;
			ews.Cells[row, col].Value = "Информация по оплате";
			ews.Cells[row, col].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
			row++;
			(Enumerable.Range(0, headers.Count())).ToList().ForEach(c =>
			{
				ews.Cells[row, col + c].Value = headers[c];
				ews.Cells[row, col + c].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
			});
			#endregion

			#region Data table
			s.ForEach(k =>
			{
				row++;
				ews.Cells[row, col].Value = $"{k.DateStamp.Value.ToShortDateString()} {k.DateStamp.Value.ToLongTimeString()}";
				ews.Cells[row, col + 1].Value = k.Cash.ToString("C2");
				ews.Cells[row, col + 2].Value = k.Details;
			});
			var table = ews.Cells[rowHeader, col, row, col + headers.Count() - 1];
			table.AutoFitColumns();
			table.Style.Border.Top.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Left.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Right.Style = ExcelBorderStyle.Thin;
			table.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
			#endregion
			return row - rowHeader;
		}
	}
}
