﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace EFRemont
{
	public static class Sup
	{
		public static ObservableCollection<T> ToObservable<T>(this List<T> list)
			=> new ObservableCollection<T>(list);
	}
	public class SelectedItemToContentConverter : IMultiValueConverter
	{
		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			// first value is selected menu item, second value is selected option item
			if (values != null && values.Length > 1)
			{
				return values[0] ?? values[1];
			}
			return null;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			return targetTypes.Select(t => Binding.DoNothing).ToArray();
		}
	}
	public class CustomHamburgerMenuIconItem : HamburgerMenuIconItem
	{
		public static readonly DependencyProperty ToolTipProperty
			= DependencyProperty.Register("ToolTip",
				typeof(object),
				typeof(CustomHamburgerMenuIconItem),
				new PropertyMetadata(null));

		public object ToolTip
		{
			get { return (object)GetValue(ToolTipProperty); }
			set { SetValue(ToolTipProperty, value); }
		}
	}
}
