﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace EFRemont
{
	public static class ClassExtension
	{
		public static async void ThrowLog(this Exception ex, string MsgInfo, MetroWindow _Window, bool IsClosed = false)
		{
			var settings = new MetroDialogSettings
			{
				AffirmativeButtonText = "Ok",
				AnimateShow = false,
				AnimateHide = false
			};
			await _Window.ShowMessageAsync("Ремонт и обслуживание",
				$"Произошла ошибка при {MsgInfo}\n\n" +
				$"Дополнительная информация:\n" +
				$"{ex.Message}\n\n" +
				$"{(IsClosed ? "Программа будет закрыта!" : "")}",
				MessageDialogStyle.Affirmative,
				settings);
		}
		public static List<AboutRequestReport> AbuseReportQuery(this List<Request> list)
		{
			return list.Select(x => new AboutRequestReport
			{
				Id = x.Id,
				Status = x.Status == 1 ? "Открыт" : "Закрыт",
				Client = x.Client.Name,
				PrePayment = x.PrePayment,
				SumPayAll = x.SumPayAll,
				CreateDate = x.CreateDate
			}).ToList();
		}

		public static DispatcherOperation DispatcherAction(this Task task, MainWindow context, Action action)
			=> context.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new ThreadStart(action));

		public static ObservableCollection<T> ToObservable<T>(this IList<T> list) => new ObservableCollection<T>(list);
	}
}
