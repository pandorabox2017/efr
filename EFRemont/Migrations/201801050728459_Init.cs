namespace EFRemont.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CashBoxMoves",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Cash = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Details = c.String(),
                        DateStamp = c.DateTime(),
                        SFlag = c.Boolean(nullable: false),
                        RequestId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requests", t => t.RequestId)
                .Index(t => t.RequestId);
            
            CreateTable(
                "dbo.Requests",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DeviceType = c.String(),
                        DeviceSN = c.String(),
                        DeviceBrend = c.String(),
                        DeviceModel = c.String(),
                        DeviceFault = c.String(),
                        Description = c.String(),
                        Deadline = c.DateTime(nullable: false),
                        DateStart = c.DateTime(),
                        DateEnd = c.DateTime(),
                        PriceApproximate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PrePayment = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quickly = c.Boolean(nullable: false),
                        Status = c.Int(nullable: false),
                        SFlag = c.Boolean(nullable: false),
                        DateStamp = c.DateTime(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        ClientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.ClientId, cascadeDelete: true)
                .Index(t => t.ClientId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Phone = c.String(),
                        Email = c.String(),
                        Address = c.String(),
                        Details = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                        SFlag = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Works",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Count = c.Int(),
                        Description = c.String(),
                        IsWork = c.Boolean(nullable: false),
                        DateStamp = c.DateTime(),
                        CreateDate = c.DateTime(nullable: false),
                        SFlag = c.Boolean(nullable: false),
                        RequestId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Requests", t => t.RequestId)
                .Index(t => t.RequestId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CashBoxMoves", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.Works", "RequestId", "dbo.Requests");
            DropForeignKey("dbo.Requests", "ClientId", "dbo.Clients");
            DropIndex("dbo.Works", new[] { "RequestId" });
            DropIndex("dbo.Requests", new[] { "ClientId" });
            DropIndex("dbo.CashBoxMoves", new[] { "RequestId" });
            DropTable("dbo.Works");
            DropTable("dbo.Clients");
            DropTable("dbo.Requests");
            DropTable("dbo.CashBoxMoves");
        }
    }
}
