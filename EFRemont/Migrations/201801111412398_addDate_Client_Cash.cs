namespace EFRemont.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDate_Client_Cash : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CashBoxMoves", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Clients", "DateStamp", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Clients", "DateStamp");
            DropColumn("dbo.CashBoxMoves", "CreateDate");
        }
    }
}
