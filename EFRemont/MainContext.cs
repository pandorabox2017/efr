﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Threading;
using Z.EntityFramework.Plus;


namespace EFRemont
{
	using ExRequestF = Expression<Func<Request, bool>>;
	class MainContext : INotifyPropertyChanged, IDataErrorInfo
	{
		private Entity db;
#if DEBUG
		private readonly string debug_path = Path.Combine(Directory.GetCurrentDirectory(), "log.txt"); 
#endif
		private MetroWindow _mw;
		public MainContext(Entity _db, MetroWindow _window)
		{
			db = _db;
			_mw = _window;
#if DEBUG
			File.Delete(debug_path);
#endif
			InitDataContext();
		}

		public bool IsNotFirstInit = false;
		private void InitDataContext()
		{
#if DEBUG
			db.Database.Log = (s) =>
			{
				File.AppendAllText(debug_path, s);
			};
#endif
			var qRequestList = db.Requests
									.AsNoTracking()
									.Include(b => b.Works)
									.Include(b => b.Client)
									.Include(b => b.CashTransactions)
									.Where(FilterRequest)
									.OrderBy(d => d.Id)
									.Skip(0)
									.Take(50)
									.Future();

			var qRequestPage = db.Requests.DeferredCount(FilterRequest).FutureValue();

			var qRequestCount_All = db.Requests.DeferredCount(FilterRequest).FutureValue();
			var qRequestCount_Apply = db.Requests.DeferredCount(FilterRequestApply).FutureValue();
			var qRequestCount_Work = db.Requests.DeferredCount(FilterRequestWork).FutureValue();

			var qClientsList = db.Clients.Where(x => x.SFlag).Future();

			var qCountClient = db.Clients.DeferredCount(FilteringCustomer).FutureValue();
			var qClientsList__ = db.Clients
				.AsNoTracking()
				.Include(b => b.Requests)
				.Where(FilteringCustomer)
				.OrderBy(d => d.Id)
				.Skip((CurrentPageCustomer - 1) * 50)
				.Take(50)
				.Future();

			var qCountCash = db.CashBoxMove.DeferredCount(FilterCash).FutureValue();

			var qCashesList = db.CashBoxMove
				.AsNoTracking()
				.Include(b => b.Request.Client)
				.Where(FilterCash)
				.OrderBy(o => o.Id)
				.Skip((CurrentPageCashes - 1) * 50)
				.Take(50)
				.Future();

			var qCashBalance = db.CashBoxMove
				.Where(FilterCash)
				.Select(x => x.Cash)
				.DefaultIfEmpty(0)
				.DeferredSum()
				.FutureValue();

			var qRequestForCash = db.Requests
				.AsNoTracking()
				.Include(b => b.Client)
				.Where(x => x.SFlag)
				.OrderBy(x => x.Id)
				.Select(x => new SimpleRequest { Id = x.Id, Description = x.Id + " от " + x.Client.Name })
				.Future();

			_requestPage = (int)Math.Ceiling(qRequestPage.Value / 50d);

			var reqList = qRequestList.ToList();
			var cliList = qClientsList__.ToList();
			var cshList = qCashesList.ToList();

			_mw.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new ThreadStart(() =>
				{
					_thirdRequestDataView = new CollectionViewSource { Source = reqList };
					_thirdClientDataView = new CollectionViewSource { Source = cliList };
					_thirdCashDataView = new CollectionViewSource { Source = cshList };
				}));
			_count = $"Всего {qRequestCount_All.Value} заявок, из них {qRequestCount_Apply.Value} закрыто, и {qRequestCount_Work.Value} на стадии исполнения";
			_clients = qClientsList.ToList();
			_customerPage = (int)Math.Ceiling(qCountClient.Value / 50d);
			_countCustomer = qCountClient.Value;
			_countCash = qCountCash.Value;
			_cashesPage = (int)Math.Ceiling(qCountCash.Value / 50d);
			_cashBalance = qCashBalance.Value;
			_requestForCash = qRequestForCash.ToList();
			
		}

		private void UpdatePage(Action propertyUpdate, CancellationTokenSource cts)
		{
			async Task Update(Action action, CancellationToken ct)
			{
				await Task.Delay(TimeSpan.FromSeconds(1));
				ct.ThrowIfCancellationRequested();

				action();
			}

			Task.Run(() => Update(propertyUpdate, cts.Token)).ContinueWith(t => GC.Collect(), TaskContinuationOptions.OnlyOnCanceled);
		}

		CancellationTokenSource ctsRequestUpdate;
		private void UpdateRequest()
		{
			OnPropertyChange("CurrentPageRequests");
			OnPropertyChange("RequestDataView");
			OnPropertyChange("Count");
			OnPropertyChange("RequestPage");
		}
		CancellationTokenSource ctsCustomerUpdate;
		private void UpdateCustomer()
		{
			OnPropertyChange("CurrentPageCustomer");
			OnPropertyChange("ClientDataView");
			OnPropertyChange("CountCustomer");
			OnPropertyChange("cutomerPage");
		}
		CancellationTokenSource ctsCashesUpdate;
		private void UpdateCash()
		{
			OnPropertyChange("CurrentPageCashes");
			OnPropertyChange("CountCash");
			OnPropertyChange("CashDataView");
			OnPropertyChange("CashBalance");
		}

		// Start code for paginate reqiests
		#region Request paginate

		ExRequestF FilterRequest => x => x.SFlag &&
				(!_isFilteringRequest || (x.Id.ToString().Contains(_findRequest) || x.Client.Name.ToUpper().Contains(_findRequest)));
		ExRequestF FilterRequestApply => x => x.SFlag && x.Status == 0 &&
				(!_isFilteringRequest || (x.Id.ToString().Contains(_findRequest) || x.Client.Name.ToUpper().Contains(_findRequest)));
		ExRequestF FilterRequestWork => x => x.SFlag && x.Status == 1 &&
				(!_isFilteringRequest || (x.Id.ToString().Contains(_findRequest) || x.Client.Name.ToUpper().Contains(_findRequest)));

		public const int pageSize = 50;

		// For first
		private int _requestPage;
		public int RequestPage
			=> IsNotFirstInit ? (int)Math.Ceiling(db.Requests.Count(FilterRequest) / (decimal)pageSize) : _requestPage;

		public string FindRequestString
		{
			set
			{
				var isNull = string.IsNullOrWhiteSpace(value);
				_findRequest = value?.ToUpper();
				_isFilteringRequest = !isNull;
				_currentPageRequests = 1;

				IsLoadPageRequest = Visibility.Visible;
				ctsRequestUpdate?.Cancel();
				ctsRequestUpdate = new CancellationTokenSource();
				UpdatePage(UpdateRequest, ctsRequestUpdate);
			}
		}
		private string _findRequest;
		private bool _isFilteringRequest;
		
		private CollectionViewSource _thirdRequestDataView;
		public ListCollectionView RequestDataView
		{
			get
			{
				try
				{
					if (!IsNotFirstInit) return _thirdRequestDataView.View as ListCollectionView;
					if (RequestPage == 0 || CurrentPageRequests <= 0) return new ListCollectionView(new List<Request>());
					_thirdRequestDataView = new CollectionViewSource
					{
						Source = db.Requests
									.AsNoTracking()
									.Include(b => b.Works)
									.Include(b => b.Client)
									.Include(b => b.CashTransactions)
									.Where(FilterRequest)
									.OrderBy(d => d.Id)
									.Skip((CurrentPageRequests - 1) * pageSize)
									.Take(pageSize)
									.ToList()
					};
					OnPropertyChange("Count");
				}
				finally
				{
					IsLoadPageRequest = Visibility.Hidden;
				}
				return _thirdRequestDataView.View as ListCollectionView;
			}
		}

		public SortDescription RequestSort
		{
			set
			{
				_thirdRequestDataView.SortDescriptions.Clear();
				_thirdRequestDataView.SortDescriptions.Add(value);
			}
		}

		private int _currentPageRequests = 1;
		private Visibility _isLoadRequest = Visibility.Hidden;
		public Visibility IsLoadPageRequest
		{
			get => _isLoadRequest;
			set
			{
				_isLoadRequest = value;
				OnPropertyChange();
			}
		}
		public int CurrentPageRequests
		{
			get => _currentPageRequests;
			set
			{
				_currentPageRequests = value < 1 ? 1 : value > RequestPage ? RequestPage : value;

				IsLoadPageRequest = Visibility.Visible;

				ctsRequestUpdate?.Cancel();
				ctsRequestUpdate = new CancellationTokenSource();
				UpdatePage(UpdateRequest, ctsRequestUpdate);

			}
		}

		// For first
		private string _count;
		public string Count
		{
			get
			{
				if (!IsNotFirstInit) return _count;

				var all = db.Requests.DeferredCount(FilterRequest).FutureValue();
				var apply = db.Requests.DeferredCount(FilterRequestApply).FutureValue();
				var work = db.Requests.DeferredCount(FilterRequestWork).FutureValue();

				return $"Всего {all.Value} заявок, из них {apply.Value} закрыто, и {work.Value} на стадии исполнения{(_isFilteringRequest ? " с учётом фильтрации" : "")}";
			}
		}

		#endregion request paginate

		#region Request similar

		// For first
		private List<Client> _clients;
		public List<Client> Clients
			=> IsNotFirstInit ? db.Clients.Where(x => x.SFlag).ToList() : _clients;

		private Request _requestView;
		public Request Request
		{
			get => _requestView;
			set
			{
				_requestView = value;
				Resource = value.Works.Select(x => new Work
				{
					Id = x.Id,
					Name = x.Name,
					Price = x.Price,
					Count = x.Count,
					Description = x.Description,
					IsWork = x.IsWork,
					DateStamp = x.DateStamp,
					CreateDate = x.CreateDate,
					SFlag = x.SFlag
				}).ToList();
				OnPropertyChange("Request");
				OnPropertyChange("SelectClient");
				OnPropertyChange("Resource");
				OnPropertyChange("Deadline");
				OnPropertyChange("DateStart");
				OnPropertyChange("SumWorks");
			}
		}
		public DateTime? DateStart
		{
			get => Request?.DateStart;
			set
			{
				Request.DateStart = value;
				OnPropertyChange("DateStart");
			}
		}
		public DateTime? Deadline
		{
			get => Request?.Deadline;
			set
			{
				Request.Deadline = value;
				OnPropertyChange("Deadline");
			}
		}
		public Client SelectClient
		{
			get => Request?.Client;
			set
			{
				if (Request != null)
					Request.Client = value;
				OnPropertyChange("SelectClient");
			}
		}
		public decimal? SumWorks => Resource?.Sum(s => s.Sum);
        private List<Work> _resource;
		public List<Work> Resource
        {
            get => _resource;
            set
            {
                _resource = value;
                OnPropertyChange();
				OnPropertyChange("ResourceDataView");
            }
        }
		public ListCollectionView ResourceDataView
			=> new CollectionViewSource { Source = Resource }.View as ListCollectionView;

		private Work _work;
		public Work Work
		{
			get => _work;
			set
			{
				_work = value;
				OnPropertyChange();
				OnPropertyChange("WorkName");
				OnPropertyChange("WorkPrice");
				OnPropertyChange("WorkDate");
				OnPropertyChange("WorkCount");
				OnPropertyChange("IsWork");
				OnPropertyChange("IsMaterial");

			}
		}
		public string WorkName
		{
			get => Work?.Name;
			set => Work.Name = value;
		}
		public decimal? WorkPrice
		{
			get => Work?.Price ?? 0;
			set => Work.Price = value;
		}
		public DateTime? WorkDate
		{
			get => Work?.DateStamp;
			set => Work.DateStamp = value;
		}
		public int? WorkCount
		{
			get => Work?.Count;
			set => Work.Count = value;
		}
		public bool IsWork
		{
			get => Work?.IsWork ?? false;
			set => Work.IsWork = value;
		}
		public bool? IsMaterial => !Work?.IsWork;

        private RelayCommand _removeWork;
        public RelayCommand RemoveWork => _removeWork ?? (_removeWork = new RelayCommand(o =>
        {
            var list = (o as System.Collections.IList).Cast<Work>().ToList();
            list.ForEach(d => Resource.Remove(d));
            OnPropertyChange("ResourceDataView");
            OnPropertyChange("SumWorks");
        }));

        private RelayCommand _openWork;
        public RelayCommand OpenWork => _openWork ?? (_openWork = new RelayCommand(o => Work = o as Work));

		#endregion similar


		// Client data
		#region Customer

		#region paginate cutomers
		//For first
		private int _customerPage;
		public int CustomerPage => IsNotFirstInit ? (int)Math.Ceiling(db.Clients.Count(FilteringCustomer) / 50d) : _customerPage;

		private int _currentPageCustomer = 1;
		private Visibility _isPaginingCustomer = Visibility.Hidden;

		public Visibility IsLoadPageCustomer
		{
			get => _isPaginingCustomer;
			set
			{
				_isPaginingCustomer = value;
				OnPropertyChange("IsLoadPageCustomer");
			}
		}

		public int CurrentPageCustomer
		{
			get => _currentPageCustomer;
			set
			{
				_currentPageCustomer = value < 1 ? 1 : value > CustomerPage ? CustomerPage : value;

				IsLoadPageCustomer = Visibility.Visible;

				ctsCustomerUpdate?.Cancel();
				ctsCustomerUpdate = new CancellationTokenSource();
				UpdatePage(UpdateCustomer, ctsCustomerUpdate);
			}
		}
		#endregion

		#region Filter
		public string FindCustomerString
		{
			set
			{
				_findCustomer = value;
				_isFindCustomer = !string.IsNullOrWhiteSpace(value);
				_currentPageCustomer = 1;

				IsLoadPageCustomer = Visibility.Visible;
				ctsCustomerUpdate?.Cancel();
				ctsCustomerUpdate = new CancellationTokenSource();
				UpdatePage(UpdateCustomer, ctsCustomerUpdate);
			}
		}
		private string _findCustomer;
		private bool _isFindCustomer;
		#endregion filter

		private Client _client;
		public Client Client
		{
			get => _client;
			set
			{
				_client = value;
				OnPropertyChange("Client");
				OnPropertyChange("ClientName");
			}
		}
		public string ClientName
		{
			get => Client?.Name;
			set
			{
				Client.Name = value;
				OnPropertyChange("ClientName");
			}
		}
		public int ClientId => Client?.Id ?? 0;
		Expression<Func<Client, bool>> FilteringCustomer => (x) => x.SFlag &&
			(_isFindCustomer ? (x.Id.ToString().Contains(_findCustomer) || x.Name.ToUpper().Contains(_findCustomer)
				   || x.Email.ToUpper().Contains(_findCustomer) || x.Address.ToUpper().Contains(_findCustomer)
				   || x.Phone.ToUpper().Contains(_findCustomer)) : true);

	
		// For first
		private CollectionViewSource _thirdClientDataView;
		public ListCollectionView ClientDataView
		{
			get
			{
				try
				{
					if (!IsNotFirstInit) return _thirdClientDataView.View as ListCollectionView;
					if (CustomerPage == 0 || CurrentPageCustomer <= 0) return new ListCollectionView(new List<Client>());
					_thirdClientDataView = new CollectionViewSource
					{
						Source = db.Clients
							.AsNoTracking()
							.Include(b => b.Requests)
							.Where(FilteringCustomer)
							.OrderBy(d => d.Id)
							.Skip((CurrentPageCustomer - 1) * 50)
							.Take(50)
							.ToList()
					};
				}
				finally
				{
					IsLoadPageCustomer = Visibility.Hidden;
				}
				return _thirdClientDataView.View as ListCollectionView;
			}
		}

		public SortDescription ClientSort
		{
			set
			{
				_thirdClientDataView.SortDescriptions.Clear();
				_thirdClientDataView.SortDescriptions.Add(value);
			}
		}

		// For fist
		private int _countCustomer;
		public string CountCustomer => $"Всего клиентов: {(IsNotFirstInit ? db.Clients.Count(FilteringCustomer) : _countCustomer)}";
		#endregion Client

		// Cash box moves
		#region CashBox

		// For first
		private int _countCash;
		public string CountCash => $"Всего транзакций: {(IsNotFirstInit ? db.CashBoxMove.Count(FilterCash) : _countCash)}";

		private bool _isFilteringCash;
		private string _findCash;
		public string FilterCashString
		{
			get => _findCash;
			set
			{
				_findCash = value;
				_isFilteringCash = !string.IsNullOrWhiteSpace(value);
				_currentPageCashes = 1;

				IsLoadPageCashes = Visibility.Visible;
				ctsCashesUpdate?.Cancel();
				ctsCashesUpdate = new CancellationTokenSource();
				UpdatePage(UpdateCash, ctsCashesUpdate);
			}
		}
		Expression<Func<CashBoxMove, bool>> FilterCash => x => x.SFlag &&
				(!_isFilteringCash || x.Id.ToString().Contains(_findCash) 
				                   || x.RequestId.ToString().Contains(_findCash)
				                   || x.Request.Client.Name.Contains(_findCash));

		// For first
		private int _cashesPage;
		public int CashesPage => IsNotFirstInit ? (int)Math.Ceiling(db.CashBoxMove.Count(FilterCash) / 50d) : _cashesPage;
		private Visibility _isLoadCashes = Visibility.Hidden;
		public Visibility IsLoadPageCashes
		{
			get => _isLoadCashes;
			set
			{
				_isLoadCashes = value;
				OnPropertyChange("IsLoadPageCashes");
			}
		}
		private int _currentPageCashes = 1;
		public int CurrentPageCashes
		{
			get => _currentPageCashes;
			set
			{
				_currentPageCashes = value < 1 ? 1 : value > CashesPage ? CashesPage : value;

				OnPropertyChange("CurrentPageCashes");

				IsLoadPageCashes = Visibility.Visible;

				ctsCashesUpdate?.Cancel();
				ctsCashesUpdate = new CancellationTokenSource();
				UpdatePage(UpdateCash, ctsCashesUpdate);
			}
		}

		// For first
		private CollectionViewSource _thirdCashDataView;
		public ListCollectionView CashDataView
		{
			get
			{
				try
				{
					if (!IsNotFirstInit) return _thirdCashDataView.View as ListCollectionView;
					if (CashesPage == 0 || CurrentPageCashes <= 0) return new ListCollectionView(new List<CashBoxMove>());
					_thirdCashDataView = new CollectionViewSource
					{
						Source = db.CashBoxMove
							.AsNoTracking()
							.Include(b => b.Request.Client)
							.Where(FilterCash)
							.OrderBy(o => o.Id)
							.Skip((CurrentPageCashes - 1) * 50)
							.Take(50)
							.ToList()
					};
					OnPropertyChange("CashBalance");
					OnPropertyChange("CashesPage");
					OnPropertyChange("CountCash");
				}
				finally
				{
					IsLoadPageCashes = Visibility.Hidden;
				}
				return _thirdCashDataView.View as ListCollectionView;
			}
		}

		public SortDescription CashSort
		{
			set
			{
				_thirdCashDataView.SortDescriptions.Clear();
				_thirdCashDataView.SortDescriptions.Add(value);
			}
		}

		// For first
		private decimal? _cashBalance;
		public decimal? CashBalance => IsNotFirstInit ? db.CashBoxMove.Where(FilterCash).Select(x => x.Cash).DefaultIfEmpty(0).Sum() : _cashBalance;

		private CashBoxMove _cash;
		public CashBoxMove Cash
		{
			get => _cash;
			set
			{
				_cash = value;
				SelectRequestForCash = _requestForCash.FirstOrDefault(x => x.Id == value.RequestId);
				OnPropertyChange("Cash");
				OnPropertyChange("CashCash");
			}
		}
		public decimal CashCash
		{
			get => Cash?.Cash ?? 0;
			set
			{
				Cash.Cash = value;
				OnPropertyChange();
			}
		}

		private SimpleRequest _selectRequestForCash;
		public SimpleRequest SelectRequestForCash
		{
			get => _selectRequestForCash;
			set
			{
				_selectRequestForCash = value;
				OnPropertyChange();
			}
		}
		private List<SimpleRequest> _requestForCash;
		public List<SimpleRequest> RequestsForCash
		{
			get
			{
				if (!IsNotFirstInit) return _requestForCash;
				SelectRequestForCash = null;
				_requestForCash = db.Requests
						.AsNoTracking()
						.Include(b => b.Client)
						.Where(x => x.SFlag)
						.OrderBy(x => x.Id)
						.Select(x => new SimpleRequest { Id = x.Id, Description = x.Id + " от " + x.Client.Name })
						.ToList();
				return _requestForCash;
			}
		}
		#endregion

		#region Report

		private bool _isDetailEnable = false;
		public bool IsDetailEnable
		{
			get => _isDetailEnable;
			set
			{
				_isDetailEnable = value;
				OnPropertyChange();
			}
		}

		private DateTime? _reportDateStart;
		public DateTime? ReportDateStart
		{
			get => _reportDateStart;
			set
			{
				_reportDateStart = value;
				OnPropertyChange();
				if (value > ReportDateEnd) ReportDateEnd = value;
			}
		}

		private DateTime? _reportDateEnd;
		public DateTime? ReportDateEnd
		{
			get => _reportDateEnd;
			set
			{
				_reportDateEnd = value;
				OnPropertyChange();
			}
		}

		#endregion



		// Validate property
		public string this[string columnName]
		{
			get
			{
				string err = string.Empty;

				switch (columnName)
				{
					#region Work
					case "WorkName":
						err = string.IsNullOrWhiteSpace(WorkName) ? "Введите наименование" : string.Empty;
						break;
					case "WorkPrice":
						err = WorkPrice is null ? "Введите цену" : string.Empty;
						break;
					case "WorkDate":
						err = WorkDate is null ? "Введите дату" : string.Empty;
						break;
					case "WorkCount":
						err = WorkCount is null ? "Введите количество" : string.Empty;
						break;
					#endregion Work

					#region Request
					case "SelectClient":
						err = SelectClient is null ? "Выберите клиента" : string.Empty;
						break;
					case "Deadline":
						err = Deadline is null ? "Введите дату дедлайна" : string.Empty;
						break;
					case "DateStart":
						err = DateStart is null ? "Введите дату открытия" : string.Empty;
						break;
					#endregion

					#region Client
					case "ClientName":
						err = string.IsNullOrWhiteSpace(ClientName) ? "Введите наименование" : string.Empty;
						break;
					#endregion

					#region
					case "CashCash":
						err = CashCash == 0 ? "Введите сумму операции" : string.Empty;
						break;
					case "SelectRequestForCash":
						err = SelectRequestForCash is null ? "Выберите заказ" : string.Empty;
						break;
					#endregion
				}

				return err;
			}
		}

		public string Error => String.Empty;

		public event PropertyChangedEventHandler PropertyChanged = delegate { };
		
		public void OnPropertyChange([CallerMemberName] string name = "") => PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
	}
	public class SimpleRequest
	{
		public int Id { get; set; }
		public string Description { get; set; }
	}
}
