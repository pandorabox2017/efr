﻿using System;
using System.ComponentModel;

namespace EFRemont
{
	public class AboutRequestReport
	{
		[DisplayName("Номер")]
		public int Id { get; set; }
		[DisplayName("Статус заказа")]
		public string Status { get; set; }
		[DisplayName("Клиент")]
		public string Client { get; set; }
		[DisplayName("Предоплата")]
		public decimal PrePayment { get; set; }
		[DisplayName("Выплаченная сумма")]
		public decimal SumPayAll { get; set; }
		[DisplayName("Дата оформления заказа")]
		public DateTime CreateDate { get; set; }

		//[DisplayName("Срочно")]
		//public string Quick { get; set; }
		//[DisplayName("Сумма заказа")]
		//public decimal TotalSum { get; set; }
		//[DisplayName("Дата оформления")]
		//public DateTime? DateStart { get; set; }
		//[DisplayName("Крайний срок")]
		//public DateTime? Deadline { get; set; }
		//[DisplayName("Дата закрытия")]
		//public DateTime? DateEnd { get; set; }
		//[DisplayName("Тип устройства")]
		//public string DeviceType { get; set; }
		//[DisplayName("Серийный номер")]
		//public string DeviceSN { get; set; }
		//[DisplayName("Бренд")]
		//public string DeviceBrend { get; set; }
		//[DisplayName("Модель")]
		//public string DeviceModel { get; set; }
		//[DisplayName("Неисправность")]
		//public string DeviceFault { get; set; }
		//[DisplayName("Описание")]
		//public string Description { get; set; }
		//[DisplayName("Ориентировочная цена")]
		//public decimal PriceApproximate { get; set; }
		//[DisplayName("Сумма по транзакциям")]
		//public decimal SumPay { get; set; }
		//[DisplayName("Сумма доп. услуг")]
		//public decimal SumWorks { get; set; }
		//[DisplayName("Последнее изменение заказа")]
		//public DateTime DateStamp { get; set; }
	}
}