﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Threading;
using System.Data.Entity;
using Z.EntityFramework.Plus;
using System.ComponentModel;
using System.Windows.Threading;

using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System.Threading.Tasks;
using System.Linq.Expressions;
using OfficeOpenXml;
using System.IO;
using Microsoft.Win32;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Table;

namespace EFRemont
{
	public partial class MainWindow
	{
		private Entity db;
		public MainWindow()
		{
			InitializeComponent();

			(new Thread(ConnectDB)).Start();
		}

		private async void ConnectDB()
		{
			try
			{
				db = new Entity();
				if (!TestConnection(db)) throw new Exception("Нет доступа к серверу");
				var context = new MainContext(db, this);

				await Dispatcher.BeginInvoke(DispatcherPriority.Normal, new ThreadStart(() =>
					{
						GridRequest.DataContext
							= GridCustomer.DataContext
							= GridCash.DataContext
							= GridReport.DataContext
							= DataContext = context;
						context.IsNotFirstInit = true;
						Waiter.Visibility = Visibility.Collapsed;
					}));
			}
			catch (Exception ex)
			{
				MessageBox.Show($"Ошибка использования базы данных.\nПрограмма будет закрыта\n\nДоп. информация:\n{ex.Message}",
					"Управление заявками", MessageBoxButton.OK, MessageBoxImage.Stop);
				Environment.Exit(Environment.ExitCode);
			}
		}

		private bool TestConnection(DbContext _db)
		{
			try
			{
				_db.Database.Connection.Open();
				return true;
			}
			catch { return false; }
		}

		private void CreateRequestClick(object sender, RoutedEventArgs e)
		{
			((MainContext)DataContext).Request = new Request();
			Fly.IsOpen = true;
		}

		// Open request
		private void LvRequestOpenSelectClick(object sender, MouseButtonEventArgs e)
		{
			if (lvRequest.SelectedItem is Request siRequest)
			{
				try
				{
					var request = db.Requests
								.Include(b => b.Client)
								.Include(b => b.Works)
								.Include(b => b.CashTransactions)
								.FirstOrDefault(x => x.SFlag && x.Id == siRequest.Id);
					db.Entry(request).State = EntityState.Unchanged;
					var ctx = (MainContext)DataContext;
					ctx.Work = new Work();
					ctx.Request = request;
					Fly.IsOpen = true;
				}
				catch (Exception ex)
				{
					ex.ThrowLog("откытии заказа", this);
				}
			}
		}

		private void AddWork(object sender, RoutedEventArgs e)
		{
			if (IsValidWork())
			{
                var ctx = DataContext as MainContext;
                if (ctx.Work?.Id == 0)
                {
                    ctx.Resource.Add(ctx.Work);
                    ctx.OnPropertyChange("Resource");
                    ctx.OnPropertyChange("SumWorks"); 
                }
                else
                {
                    var _work = ctx.Resource.FirstOrDefault(x => x.Id == ctx.Work?.Id);
                    _work = ctx.Work;
                    ctx.OnPropertyChange("Resource");
                    ctx.OnPropertyChange("SumWorks");
                    ctx.Work = new Work();
                }
				ctx.OnPropertyChange("ResourceDataView");
			}
		}

		private bool IsValidWork()
		{
			var model = new List<BindingExpression>
			{
				workName.GetBindingExpression(TextBox.TextProperty),
				workPrice.GetBindingExpression(TextBox.TextProperty),
				workDate.GetBindingExpression(DatePicker.SelectedDateProperty),
				workCount.GetBindingExpression(NumericUpDown.ValueProperty)
			};
			model.ForEach(m => m.UpdateSource());
			var errElement = model.FirstOrDefault(x => x.HasError)?.Target as FrameworkElement;
			errElement?.Focus();
			return errElement is null;
		}

		private void ChangeRequest(object sender, RoutedEventArgs e)
		{
			if (IsValidRequest())
			{
				var ctx = (MainContext)DataContext;
				var model = ctx.Request;
				var works = ctx.Resource.Select(n => new Work
				{
					Count = n.Count,
					CreateDate = n.CreateDate,
					DateStamp = n.DateStamp,
					Description = n.Description,
					IsWork = n.IsWork,
					Name = n.Name,
					Price = n.Price,
					SFlag = true
				}).ToList();
				using (var transact = db.Database.BeginTransaction())
				{
					try
					{
						if (!(model.Works is null))
							db.Works.RemoveRange(model.Works);
						model.Works = works;

						db.Requests.Attach(model);
						model.DateStamp = DateTime.Now;
						db.Entry(model).State = EntityState.Modified;
						db.SaveChanges();
						transact.Commit();
						ctx.OnPropertyChange("RequestDataView");
						Fly.IsOpen = false;
					}
					catch (Exception ex)
					{
						transact.Rollback();
						ex.ThrowLog("изменении заказа", this);
					}
				}
			}
		}

		private bool IsValidRequest()
		{
			var model = new List<BindingExpression>
			{
				cbClient.GetBindingExpression(ComboBox.SelectedValueProperty),
				dpDeadline.GetBindingExpression(DatePicker.SelectedDateProperty),
				dpDateStart.GetBindingExpression(DatePicker.SelectedDateProperty)
			};
			model.ForEach(m => m.UpdateSource());
			var errElement = model.FirstOrDefault(x => x.HasError)?.Target as FrameworkElement;
			errElement?.Focus();
			return errElement is null;
		}

		private void CreateRequest(object sender, RoutedEventArgs e)
		{
			if (IsValidRequest())
			{
				var ctx = ((MainContext)DataContext);
				var request = ctx.Request;
				var works = ctx.Resource.Select(n => new Work
				{
					Count = n.Count,
					CreateDate = n.CreateDate,
					DateStamp = n.DateStamp,
					Description = n.Description,
					IsWork = n.IsWork,
					Name = n.Name,
					Price = n.Price,
					SFlag = true
				}).ToList();
				using (var transact = db.Database.BeginTransaction())
				{
					try
					{
						request.Works = works;
						db.Requests.Add(request);
						db.SaveChanges();
						transact.Commit();
						ctx.OnPropertyChange("RequestDataView");
						Fly.IsOpen = false;
					}
					catch (Exception ex)
					{
						transact.Rollback();
						ex.ThrowLog("создании заказа", this);
					}
				}
			}
		}

		private async void DeleteRequest(object sender, RoutedEventArgs e)
		{
			var request = ((MainContext)DataContext).Request;
			if (await MsgBox($"Удалить заказ под номером {request.Id}\nКлиент: {request.Client.Name}\n\nПредварительная цена заказа: {request.PriceApproximate:C2}" +
				$"\n\nОплачено: {request.SumPayAll:C2}" +
				$"\n\nТекущий статус: {(request.Status == 1 ? "В работе" : "Закрыт")}", 2))
			{
				try
				{
					using (var transaction = db.Database.BeginTransaction())
					{
						try
						{
							db.Requests.Where(x => x.Id == request.Id).Update(r => new Request { SFlag = false });

							transaction.Commit();
							((MainContext)DataContext).OnPropertyChange("RequestDataView");
							Fly.IsOpen = false;
						}
						catch (Exception ex)
						{
							transaction.Rollback();
							throw ex;
						}
					}
				}
				catch (Exception ex)
				{
					ex.ThrowLog("удалении заказа", this);
				}
			}
		}

		private ListSortDirection _requestSortDirection;
		private GridViewColumnHeader _requestSortColumn;

		private void Request_Header_Click(object sender, RoutedEventArgs e)
		{
			var column = e.OriginalSource as GridViewColumnHeader;
			if (column is GridViewColumnHeader)
				SortedColumn(column, ref _requestSortColumn, ref _requestSortDirection, sender);
		}

		private ListSortDirection _clientSortDirection;
		private GridViewColumnHeader _clientSortColumn;

		private void Client_Header_Click(object sender, RoutedEventArgs e)
		{
			var col = e.OriginalSource as GridViewColumnHeader;
			if (col is GridViewColumnHeader)
				SortedColumn(col, ref _clientSortColumn, ref  _clientSortDirection, sender);
		}
		private ListSortDirection _cashSortDirection;
		private GridViewColumnHeader _cashSortColumn;
		private void Cash_Header_Click(object sender, RoutedEventArgs e)
		{
			var col = e.OriginalSource as GridViewColumnHeader;
			if (col is GridViewColumnHeader)
				SortedColumn(col, ref _cashSortColumn, ref _cashSortDirection, sender);
		}

		public void SortedColumn(GridViewColumnHeader col, ref GridViewColumnHeader colPrevious, ref ListSortDirection direct, object send)
		{
			var asc = ListSortDirection.Ascending;
			var desc = ListSortDirection.Descending;

			if (col == colPrevious) direct = direct == asc ? desc : asc;
			else
			{
				if (colPrevious is GridViewColumnHeader)
				{
					colPrevious.Column.HeaderTemplate = null;
					colPrevious.Column.Width = colPrevious.ActualWidth - 20;
				}
				colPrevious = col;
				direct = asc;
				col.Column.Width = col.ActualWidth + 20;
			}
			col.Column.HeaderTemplate = this.Resources[direct == asc ? "ArrowUp" : "ArrowDown"] as DataTemplate;

			var str = string.Empty;
			str = col.Column.DisplayMemberBinding is Binding b
				? b.Path.Path
				: col.Tag.ToString();
			var ctx = ((MainContext)DataContext);
			var name = (send as ListView)?.Name;
			var srtDescription = new SortDescription(str, direct);

			if (name == lvRequest.Name) ctx.RequestSort = srtDescription;
			else if (name == lvClients.Name) ctx.ClientSort = srtDescription;
			else if (name == lvCashMove.Name) ctx.CashSort = srtDescription;

		}

		private void NewClientClick(object sender, RoutedEventArgs e)
		{
			((MainContext)DataContext).Client = new Client();
			FlyClient.IsOpen = true;
		}

		private void LvClientsOpenSelectClick(object sender, MouseButtonEventArgs e)
		{
			if (!(lvClients.SelectedItem is Client siClient)) return;
			var client = db.Clients.Include(r => r.Requests).FirstOrDefault(x => x.SFlag && x.Id == siClient.Id);
			if (client != null)
			{
				db.Entry(client).State = EntityState.Unchanged;
				((MainContext)DataContext).Client = client;
				FlyClient.IsOpen = true; 
			}
		}

		private async void DeleteClient(object sender, RoutedEventArgs e)
		{
			var client = ((MainContext)DataContext).Client;
			if (!await MsgBox($"Удалить клиента {client.Name} под номером {client.Id}?", 2)) return;
			try
			{
				using (var transaction = db.Database.BeginTransaction())
				{
					try
					{
						db.Requests.Where(x => x.SFlag && x.Client.Id == client.Id).Update(u => new Request { SFlag = false });
						db.CashBoxMove.Where(x => x.SFlag && x.Request.Client.Id == client.Id).Update(u => new CashBoxMove { SFlag = false });
						db.Clients.Where(x => x.Id == client.Id).Update(r => new Client { SFlag = false });
						transaction.Commit();

						((MainContext)DataContext).OnPropertyChange("Clients");
						((MainContext)DataContext).OnPropertyChange("ClientDataView");
						FlyClient.IsOpen = false;
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				ex.ThrowLog("удалении клиента", this);
			}
		}

		private void ChangeClient(object sender, RoutedEventArgs e)
		{
			if (!IsValidClient()) return;
			var ctx = ((MainContext)DataContext);
			var client = ctx.Client;
			try
			{
				using (var transact = db.Database.BeginTransaction())
				{
					try
					{
						client.DateStamp = DateTime.Now;
						db.Entry(client).State = EntityState.Modified;
						db.SaveChanges();
						transact.Commit();
						ctx.OnPropertyChange("ClientDataView");
						ctx.OnPropertyChange("Clients");
						FlyClient.IsOpen = false;
					}
					catch (Exception ex)
					{
						transact.Rollback();
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				ex.ThrowLog("изменении клиента", this);
			}
		}

		private void CreateClient(object sender, RoutedEventArgs e)
		{
			if (!IsValidClient()) return;
			var ctx = ((MainContext)DataContext);
			var client = ctx.Client;
			try
			{
				using (var transact = db.Database.BeginTransaction())
				{
					try
					{
						db.Clients.Add(client);
						db.SaveChanges();
						transact.Commit();
						ctx.OnPropertyChange("ClientDataView");
						ctx.OnPropertyChange("Clients");
						FlyClient.IsOpen = false;
					}
					catch (Exception ex)
					{
						transact.Rollback();
						throw ex;
					}
				}
			}
			catch (Exception ex)
			{
				ex.ThrowLog("нового клиента", this);
			}
		}

		private bool IsValidClient()
		{
			var be = NameClient.GetBindingExpression(TextBox.TextProperty);
			be.UpdateSource();
			if (be.HasError) (NameClient as FrameworkElement).Focus();
			return !be.HasError;
		}

		private bool IsValidCash()
		{
			var be = new List<BindingExpression>
			{
				cbCashReport.GetBindingExpression(ComboBox.SelectedItemProperty),
				CashBoxSum.GetBindingExpression(TextBox.TextProperty)
			};
			be.ForEach(m => m.UpdateSource());
			var errEl = be.FirstOrDefault(x => x.HasError)?.Target as FrameworkElement;
			errEl?.Focus();
			return errEl is null;
		}

		private void CreateCashTransaction(object sender, RoutedEventArgs e)
		{
			if (IsValidCash())
			{
				var ctx = ((MainContext)DataContext);
				var cash = ctx.Cash;
				try
				{
					using (var transact = db.Database.BeginTransaction())
					{
						try
						{
							cash.Request = db.Requests.FirstOrDefault(x => x.Id == ctx.SelectRequestForCash.Id && x.SFlag);
							db.CashBoxMove.Add(cash);
							db.SaveChanges();
							transact.Commit();
							ctx.OnPropertyChange("CashDataView");
							FlyCash.IsOpen = false;
						}
						catch (Exception ex)
						{
							transact.Rollback();
							throw ex;
						}
					}
				}
				catch (Exception ex)
				{
					ex.ThrowLog("добавлении новой транзакции", this);
				}
			}
		}

		private void ChangeCashTransaction(object sender, RoutedEventArgs e)
		{
			if (IsValidCash())
			{
				var ctx = ((MainContext)DataContext);
				var cash = ctx.Cash;
				try
				{
					using (var transact = db.Database.BeginTransaction())
					{
						try
						{
							cash.Request = db.Requests.FirstOrDefault(x => x.Id == ctx.SelectRequestForCash.Id && x.SFlag);
							db.Entry(cash).State = EntityState.Modified;
							db.SaveChanges();
							transact.Commit();
							ctx.OnPropertyChange("Cashes");
							FlyCash.IsOpen = false;
						}
						catch (Exception ex)
						{
							transact.Rollback();
							throw ex;
						}
					}
				}
				catch (Exception ex)
				{
					ex.ThrowLog("изменении транзакции", this);
				}
			}
		}

		private async void DeleteCashTransaction(object sender, RoutedEventArgs e)
		{
			var cash = ((MainContext)DataContext).Cash;
			if(await MsgBox($"Удалить транзакцию #{cash.Id}, заказ #{cash.RequestId} от клиента: {cash.Request.Client.Name}" +
				$"\n\nСумма операции: {cash.Cash.ToString("C2")}" +
				$"\n\nДата исполнения: {cash.DateStamp.Value.ToLongDateString()}" +
				$"\nВремя исполнения: {cash.DateStamp.Value.ToLongTimeString()}", 2))
			{
				try
				{
					using (var transaction = db.Database.BeginTransaction())
					{
						try
						{
							db.CashBoxMove.Where(x => x.Id == cash.Id).Update(r => new CashBoxMove { SFlag = false });
							transaction.Commit();

							((MainContext)DataContext).OnPropertyChange("Cashes");
							FlyCash.IsOpen = false;
						}
						catch (Exception ex)
						{
							transaction.Rollback();
							throw ex;
						}
					}
				}
				catch (Exception ex)
				{
					ex.ThrowLog("удалении транзакции", this);
				}
				
			}
		}

		private void LvCashesOpenSelectClick(object sender, MouseButtonEventArgs e)
		{
			if (lvCashMove.SelectedItem is CashBoxMove siCash)
			{
				var cash = db.CashBoxMove
					.Include(b => b.Request)
					.Include(b => b.Request.Client)
					.FirstOrDefault(x => x.SFlag && x.Id == siCash.Id);
				db.Entry(cash).State = EntityState.Unchanged;
				((MainContext)DataContext).Cash = cash;
				FlyCash.IsOpen = true;
			}
		}

		private void AddCash(object sender, RoutedEventArgs e)
		{
			((MainContext)DataContext).Cash = new CashBoxMove();
			((MainContext)DataContext).OnPropertyChange("RequestsForCash");
			FlyCash.IsOpen = true;
		}

		public async Task<bool> MsgBox(string Message, int TypeDialogOk = 1)
		{

			var settings = 
				TypeDialogOk == 1
				? new MetroDialogSettings
				{
					AffirmativeButtonText = "Ok"
				}
				: TypeDialogOk == 2 
				? new MetroDialogSettings
				{
					AffirmativeButtonText = "Да",
					NegativeButtonText = "Нет",

				} : null;
			var style = TypeDialogOk == 1 ? MessageDialogStyle.Affirmative : MessageDialogStyle.AffirmativeAndNegative;

			settings.ColorScheme = MetroDialogOptions.ColorScheme;
			settings.AnimateShow = false; settings.AnimateHide = false;

			var result = await this.ShowMessageAsync("Ремонт и обслуживание", Message, style, settings);

			return result == MessageDialogResult.Affirmative;
		}
		
		private void cbClient_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cbClient.Text))
			{
				cbClient.Items.Filter = null;
				cbClient.IsDropDownOpen = false;
			}
		}

		private void cbClient_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter || e.Key == Key.Tab)
				cbClient.Items.Filter = null;
			else
			{
				cbClient.Items.Filter += (o) => ((Client)o).Name.Contains(cbClient.Text);
				cbClient.IsDropDownOpen = true;
			}
		}

		private void cbCashReport_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Enter || e.Key == Key.Tab)
				cbCashReport.Items.Filter = null;
			else
			{
				cbCashReport.Items.Filter += (o) => ((SimpleRequest)o).Description.Contains(cbCashReport.Text);
				cbCashReport.IsDropDownOpen = true;
			}
		}

		private void cbCashReport_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (string.IsNullOrWhiteSpace(cbCashReport.Text))
			{
				cbCashReport.Items.Filter = null;
				cbCashReport.IsDropDownOpen = false;
			}
		}

		DateTime? startPeriod => dtRepStart.SelectedDate;
		DateTime? endPeriod=> dtRepEnd.SelectedDate;
		string search;
		bool isSearch;
		int requestState => cbRequestReportState.SelectedIndex;
		int typeReport => tabReportType.SelectedIndex;


		private Expression<Func<Request, bool>> FilterRequest => r => r.SFlag
					&& (!isSearch || (r.Id.ToString().Contains(search) || r.Client.Name.ToUpper().Contains(search)))
					&& (!startPeriod.HasValue || (r.DateStart > startPeriod || r.DateEnd > startPeriod))
					&& (!endPeriod.HasValue || (r.DateStart < endPeriod || r.DateEnd < endPeriod))
					&& (requestState == 1 ? (r.Status == 1) :
						requestState != 2 || (r.Status == 0));
		private Expression<Func<Client, bool>> FilterClient => x => x.SFlag
					&& (!isSearch || (x.Id.ToString().Contains(search) || x.Name.ToUpper().Contains(search)
					                                                   || x.Email.ToUpper().Contains(search) || x.Address.ToUpper().Contains(search)
					                                                   || x.Phone.ToUpper().Contains(search)))
					&& (!startPeriod.HasValue || (x.DateStamp > startPeriod))
					&& (!endPeriod.HasValue || (x.DateStamp < endPeriod));

		private void CreateReport(object sender, RoutedEventArgs e)
		{
			search = txtSearchReport.Text.ToUpper();
			isSearch = !string.IsNullOrWhiteSpace(search);

			GridView gridView = null;

			if (typeReport == 0)
			{
				gridView = new GridView();
				gridView.Columns.Add(new GridViewColumn { Header = "#",					DisplayMemberBinding = new Binding("Id") });
				gridView.Columns.Add(new GridViewColumn { Header = "Клиент",			DisplayMemberBinding = new Binding("Client.Name") });
				gridView.Columns.Add(new GridViewColumn { Header = "Начало",			DisplayMemberBinding = new Binding("DateStart") });
				gridView.Columns.Add(new GridViewColumn { Header = "Завершение",		DisplayMemberBinding = new Binding("DateEnd") });
				gridView.Columns.Add(new GridViewColumn { Header = "Аванс",				DisplayMemberBinding = new Binding("PrePayment") });
				gridView.Columns.Add(new GridViewColumn { Header = "Крайний срок",		DisplayMemberBinding = new Binding("Deadline") });
				gridView.Columns.Add(new GridViewColumn { Header = "Оплачено",			DisplayMemberBinding = new Binding("SumPayAll") });
				gridView.Columns.Add(new GridViewColumn { Header = "Тип устройства",	DisplayMemberBinding = new Binding("DeviceType") });

				db.Requests
					.AsNoTracking()
					.Include(i => i.Client)
					.Include(i => i.CashTransactions)
					.Where(FilterRequest)
					.OrderBy(x => x.Id)
					.Take(70)
					.ToListAsync()
					.ContinueWith(task => task.DispatcherAction(this, () => lvReport.ItemsSource = task.Result));
			}
			else if (typeReport == 1)
			{
				var isViewRequest = IsRequestForClientsToReport.IsChecked;
				var isDetailRequest = IsDetailRequestForClientsToReport.IsChecked;

				gridView = new GridView();
				gridView.Columns.Add(new GridViewColumn { Header = "#",				DisplayMemberBinding = new Binding("Id") });
				gridView.Columns.Add(new GridViewColumn { Header = "Наименование",	DisplayMemberBinding = new Binding("Name") });
				gridView.Columns.Add(new GridViewColumn { Header = "Телефон",		DisplayMemberBinding = new Binding("Phone") });
				gridView.Columns.Add(new GridViewColumn { Header = "Почта",			DisplayMemberBinding = new Binding("Email") });
				gridView.Columns.Add(new GridViewColumn { Header = "Адрес",			DisplayMemberBinding = new Binding("Address") });

                db.Clients
                    .AsNoTracking()
                    .Where(FilterClient)
                    .OrderBy(x => x.Id)
                    .Take(70)
                    .ToListAsync()
                    .ContinueWith(task => task.DispatcherAction(this, () =>
                    {
                        lock (lvReport) lvReport.ItemsSource = task.Result;
                    }));
			}
			lvReport.View = gridView;
		}

		private async void ReportExport_Click(object sender, RoutedEventArgs e)
		{
			if (typeReport == 0)
			{
				var file = new SaveFileDialog
				{
					DefaultExt = "xlsx",
					AddExtension = true,
					CheckPathExists = true,
					Filter = "Электронные таблицы 2007 (*.xlsx)|*.xlsx",
					Title = "Укажите файл для сохранения отчёта",
					OverwritePrompt = true,
					FileName = $"Заказы_{DateTime.Now.ToShortDateString()}"
				};
				if (file.ShowDialog() is true)
				{
					(new CreateReport(db, this, file.FileName)).PublicRequests();
				} 
			}
			else
			{
				var file = new SaveFileDialog
				{
					DefaultExt = "xlsx",
					AddExtension = true,
					CheckPathExists = true,
					Filter = "Электронные таблицы 2007 (*.xlsx)|*.xlsx",
					Title = "Укажите файл для сохранения отчёта",
					OverwritePrompt = true,
					FileName = $"Клиенты_{DateTime.Now.ToShortDateString()}"
				};
				if (file.ShowDialog() is true)
				{
					(new CreateReport(db, this, file.FileName)).PublicClients();
				}
			}
		}

		private void Hamburger_ItemClick(object sender, ItemClickEventArgs e)
		{
			MenuHaburger.Content = e.ClickedItem;
			MenuHaburger.IsPaneOpen = false;
		}

	}
}
